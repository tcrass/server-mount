#!/usr/bin/env python

import argparse
from datetime import *
from glob import glob
import grp
import json
import os
import pwd
import signal
import time
import subprocess
import syslog
from syslog import syslog as log
import threading


MAX_PING_ATTEMPTS = 3

CONFIG_FILENAME = "server-mount.config.json"

CONFIG_LOCATIONS = [
    os.path.join(os.getcwd(), CONFIG_FILENAME),
    os.path.join(os.path.dirname(__file__), CONFIG_FILENAME),
    os.path.join('/etc', CONFIG_FILENAME),
]


# from https://medium.com/greedygame-engineering/an-elegant-way-to-run-periodic-tasks-in-python-61b7c477b679
class Job(threading.Thread):

    def __init__(self, interval, execute, *args, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = False
        self.stopped = threading.Event()
        self.interval = interval
        self.execute = execute
        self.args = args
        self.kwargs = kwargs


    def stop(self):
                self.stopped.set()
                self.join()


    def run(self):
            interval = self.interval.total_seconds()
            last_execution = datetime.now()
            self.execute(*self.args, **self.kwargs)
            while not self.stopped.wait(interval - (datetime.now() - last_execution).total_seconds() % interval):
                last_execution = datetime.now()
                self.execute(*self.args, **self.kwargs)
          

class TerminationRequested(Exception):
    pass


def on_termination_signal(signum, frame):
    raise TerminationRequested


def parse_command_line():
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--config-file', help="Path to config file")
    parser.add_argument('-r', '--repeat', help="Repeat interval in seconds")

    return parser.parse_args()


def read_config(args):
    if args.config_file:
        if os.path.isfile(args.config_file):
            return try_read_config_from(args.config_file)
        else:
            raise IOError(f"No such file: {args.config_file}")
    else:
        config = None
        for path in CONFIG_LOCATIONS:
            config = try_read_config_from(path)
            if config != None:
                break
        if config == None:
            raise Exception(f"No valid config file found at {CONFIG_LOCATIONS}!")
        return config


def try_read_config_from(path):
    config = None
    if os.path.isfile(path):
        try:
            with open(path) as file:
                config = json.load(file)
        except Exception as ex:
            log(f"Error while trying to read config file at {path}: {str(ex)}")
    return config


def is_reachable(network):
    command = ['ping', '-c', '1', network['host']]
    reachable = False
    attempt = 1
    while not reachable and attempt <= MAX_PING_ATTEMPTS:
        log(f"Determining reachability of {json.dumps(network)} (attempt {attempt} of max. {MAX_PING_ATTEMPTS})...")
        exitcode = subprocess.call(command)
        reachable = exitcode == 0
        attempt = attempt + 1
    log(f"...reachable: {reachable}")
    return reachable


def determine_network(networks):
    log("Determining network connection...")
    connected_to = None
    for network in networks:
        if is_reachable(network):
            connected_to = network
            break;
    log("...mot connected!" if connected_to is None else f"...connected to {json.dumps(connected_to)}!")
    return connected_to


def get_users(mountpoint):
    log("Finding users...")
    usernames = grp.getgrnam('users').gr_mem
    usernames.sort()
    users = []
    for username in usernames:
        info = pwd.getpwnam(username)
        user = {
            'name': username,
            'uid': info.pw_uid,
            'gid': info.pw_gid,
            'home': info.pw_dir,
            'mountpoint': os.path.join(info.pw_dir, mountpoint)
        }
        users.append(user)
    log(f"...found {json.dumps(users)}")
    return users


def is_mounted(username, host, mountpoint):
    log(f"Checking if {username}@{host} is mounted at {mountpoint}...")
    p1 = subprocess.Popen(['mount'], stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['grep', f'{username}@{host}', ], stdin=p1.stdout, stdout=subprocess.PIPE)
    p3 = subprocess.Popen(['grep', mountpoint], stdin=p2.stdout, stdout=subprocess.PIPE)
    out, err = p3.communicate()
    if p3.returncode == 0:
        log("...yes!")
        return True
    elif p3.returncode == 1:
        log("...no!")
        return False        
    else:
        raise Exception(syslog.LOG_ERR, f"Error determining if {username}@{host} is mounted at {mountpoint}!")        


def unmount(mountpoint):
    log(f"Unmounting {mountpoint}...")
    try:
        cmd = ['umount', '-f', '-l', mountpoint]
        log(syslog.LOG_DEBUG, f"Unmount command: {json.dumps(cmd)}")
        subprocess.check_call(cmd)
        if os.path.isdir(mountpoint) and len(os.listdir(mountpoint)) == 0:
            log(f"Removing {mountpoint}...")
            os.rmdir(mountpoint)
        log(f"...done unmounting {mountpoint}!")
    except OSError as ex:
        log(syslog.LOG_WARNING, f"Error unmonting {mountpoint}: {str(ex)}!")


def unmount_unconnected(connected_to, networks, users):
    log("Unmounting stale mounts...")
    for network in networks:
        if network != connected_to:
            for user in users:
                if is_mounted(user['name'], network['host'], user['mountpoint']):
                    unmount(user['mountpoint'])
    log("...done unmounting stale mounts!")


def mount(user, network):
    log(f"Mounting {user['name']}@{network['host']} at {user['mountpoint']}...")
    dir = user['mountpoint']
    try:
        os.makedirs(dir, mode=0x775)
    except FileExistsError:
        pass
    if os.path.isdir(dir) and len(os.listdir(dir)) == 0:
        try:
            while dir != user['home']:
                log(syslog.LOG_DEBUG, f'Chowning {dir}')
                os.chown(dir, user['uid'], user['gid'])
                dir,_ = os.path.split(dir)
            cmd = [
                'sshfs', 
                f'{user["name"]}@{network["host"]}:{network["root"]}/{user["name"]}', 
                user['mountpoint'], 
                '-o'
                f'port={network["port"]},_netdev,delay_connect,reconnect,user,idmap=user,transform_symlinks,identityfile=/home/{user["name"]}/.ssh/id_rsa,allow_other,default_permissions,uid={user["uid"]},gid={user["gid"]}'
            ]
            log(f"...done mounting {user['name']}@{network['host']} at {user['mountpoint']}!")
            log(syslog.LOG_DEBUG, f'Mount command: {json.dumps(cmd)}')
            subprocess.check_call(cmd)
        except OSError as ex:        
            log(syslog.LOG_WARNING, f"Error monting {user['name']}@{network['host']} at {user['mountpoint']}: {str(ex)}!")
    else:
        log(syslog.LOG_WARNING, f"Unable to mount {user['name']}@{network['host']} at {user['mountpoint']} because mountpoint is not an empty directory!")


def ensure_accessible(user, network):
    for attempt in range(1, 4):
        try:
            os.listdir(user['mountpoint'])
            break
        except OSError as e:
            log(f"Mountpoint {user['mountpoint']} not accessible, trying to remount (attempt {attempt})...")
            unmount(user['mountpoint'])
            mount(user, network)


def mount_connected(network, users):
    log("Mounting unmounted user mounts...")
    for user in users:
        if is_mounted(user['name'], network['host'], user['mountpoint']):
            ensure_accessible(user, network)
        else:
            mount(user, network)
    log("...done mounting unmounted user mounts!")


def execute(config):
    connected_to = determine_network(config['networks'])
    users = get_users(config['mountpoint'])
    unmount_unconnected(connected_to, config['networks'], users)
    if connected_to != None:
        mount_connected(connected_to, users)


def try_execute(config):
    try:
        execute(config)
    except Exception as ex:
        log(str(ex))


def main():
    args = parse_command_line()
    config = read_config(args)
    if args.repeat:
        signal.signal(signal.SIGTERM, on_termination_signal)
        signal.signal(signal.SIGINT, on_termination_signal)
        job = Job(timedelta(seconds=int(args.repeat)), try_execute, config)
        job.start()
        while True:
            try:
                time.sleep(1)
            except TerminationRequested:
                job.stop()
                break
    else:
        execute(config)


if __name__ == "__main__":
    main()
