# server-mount

A tool for automatically (re-)mounting all users' remote shared folders into a dedicated sub-directory within their home folders using sshfs, taking into consideration under which hostname and on which port the hosting server is reachable, depending on which network the client is currently in.

The motivation behind this tool was that I wanted our family PCs to preferably use our home server's LAN hostname, but to also be able to mount our roaming folders when e.g. a laptop is connected to the internet through some other network.

## Installation and configuration

- Put the `server-mount.py` script into `/etc/NetworkManager/dispatcher.d` and rename it to `99-server-mount.py`
- Place a copy of `server-mount.sample.json` into the same directory and rename it to `server-mount.json`
- Edit `server-mount.json` to match your situation.
  - Servers are tried from top to bottom, and the first reachable server wins.
  - The mountpoint specified will be created inside each user's home directory.
